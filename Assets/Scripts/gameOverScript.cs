﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class gameOverScript : MonoBehaviour
{
    public Canvas gameOverMenu;
    public Button mainMenuText;
    public Button exitText;
    public AudioClip _gameMenuMusic;
    public AudioClip _startAudio;
    public AudioClip _quitAudio;
    // Use this for initialization
    void Start()
    {
        gameOverMenu = gameOverMenu.GetComponent<Canvas>();
        mainMenuText = mainMenuText.GetComponent<Button>();
        exitText = exitText.GetComponent<Button>();
        GetComponent<AudioSource>().PlayOneShot(_startAudio);
        MusicManager();
    }


    void MusicManager()
    {
        GetComponent<AudioSource>().clip = _gameMenuMusic;
        GetComponent<AudioSource>().Play();
        GetComponent<AudioSource>().loop = true;
    }

    public void StartLevel()
    {

        GetComponent<AudioSource>().PlayOneShot(_startAudio);
        SceneManager.LoadScene(1);

    }

    public void ExitGame()
    {
        GetComponent<AudioSource>().PlayOneShot(_quitAudio);
        Application.Quit();
    }

    public void MainMenu()
    {
        SceneManager.LoadScene(0);
    }
    // Update is called once per frame
    void Update()
    {

    }
}
